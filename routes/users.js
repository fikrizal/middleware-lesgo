var express = require('express');
var router = express.Router();
var firebase = require('firebase');

var config = {
  apiKey: "AIzaSyCxqig6JZ95MiuI8zwfufuI-ubXwT1vcCs",
  authDomain: "lesgo-dev-v2.firebaseapp.com",
  databaseURL: "https://lesgo-dev-v2.firebaseio.com",
  projectId: "lesgo-dev-v2",
  storageBucket: "lesgo-dev-v2.appspot.com",
  messagingSenderId: "299138139948"
};
firebase.initializeApp(config);

var ref = firebase.app().database().ref();
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/test', (req, res, next)=> {
  var usersRef = ref.child('users');
  usersRef.on('value', (res)=>{
    console.log(res.val());
  })
})

router.post('/midtranshandler',function(req,res){
  console.log(req.body);
  res.end("yes");

});

module.exports = router;
